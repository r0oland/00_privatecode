//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Includes
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#include <LiquidCrystal.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// My files
#include "config.h"
#include "sensing.h"
#include "globals.h"
#include "lcd_functions.h"



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// LCD Display Setup
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(22, 23, 25, 26, 27, 28);
char buffer[6]=""; //buffer for lcd string

//

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setup() {
  // setup lcd
  lcd.begin(20, 4);
  lcd.print("ACM 0.02");

  // delay(5000); //give PC that Arduino is attaced too time to start up,

  // Interrupts
  // see http://www.arduino.cc/en/Reference/AttachInterrupt for interrupt pins
  // Mega2560:  pins: 2 3 21 20 19 18 => interrupt 0 to  5
  attachInterrupt(2, increaseFlowCounter, RISING); //attach interrupt to pin 2
  attachInterrupt(3, increaseRPMCounter, RISING); //attach interrupt to pin 3
  analogReference(EXTERNAL); //set to external reference to use the correct
  //5V signal, since it is important for temp. meas acquracy

  // PWM pins and other magic
  pinMode(FAN_PIN_1, OUTPUT);   // sets the pin as output
  //set to highest possible freq. for PWM channles  5,3,2 so you don't hear them
  // see http://forum.arduino.cc/index.php?topic=72092.0 for this
  TCCR3B = TCCR3B & 0b11111000 | 0x01;
  analogWrite(FAN_PIN_1, 3);  // analogRead values go from 0 to 1023, analogWrite values from 0 to 255

  clear_LCD();

  struct Thermistor therm1;
  therm1.SensorPin = 5;
  therm1.BParameter = 3455;
}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void loop() {
  // sensing functions
  // measure_temperature();

  // sensing functions
  // control_fan() to be implemented

  // displayFunctions();
  display_flow();
  display_uptime();
  display_RPM();
  display_temperatures();
}
