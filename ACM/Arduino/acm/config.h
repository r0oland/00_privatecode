#ifndef config_h
#define config_h

// Display Constants
const uint_least8_t CLEAR_LCD_TIMER = 10;
// clear LCD every 10 seconds in case there was a display glitch

// display update timer
const uint_least8_t RPM_REFRESH_TIME = 2;
const uint_least8_t UPTIME_REFRESH_TIME = 1;
const uint_least8_t FLOW_REFRESH_TIME = 2;
const uint_least8_t TEMPERATURE_REFRESH_TIME = 1;

// Analog Pins >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
// Thermistor Pins
const uint_least8_t THERMISTOR_PIN_0 = 5;
const uint_least8_t THERMISTOR_PIN_1 = 6;
const uint_least8_t THERMISTOR_PIN_2 = 7;
const uint_least8_t nSensors = 1;

//Digital Pins
const uint_least8_t FAN_PIN_1 = 2; //needs to be PWM pin!

// Physical and Sensor Constants
const float COUNTS_TO_LITER = 0.307/60; // see datasheet, lpm = 0.3 * Freq.

const uint_fast16_t REF_RESIST = 10000;
const uint_fast16_t THERM_RESIST = 10000; //nominal thermistor resistance
const uint_fast16_t B_PARAMETER[] = {
  3455, //home thermistor
  3455, //foil thermistor
  3455, //small ball thermistor
  3455, //smd thermistor
  }; //
// B parameter as used in Steinhart–Hart equation bit.ly/1Nn9CMy
// B-Constant (25-50°C): 3380, (25-80°C): 3428, (25-85°C): 3434, (25-100°C): 3455
const float AMBIENT_TEMP = 298.15; //room temperature in Kelvin (25 Deg default)

#endif
