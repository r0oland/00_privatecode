/*********************************************************************************
 **
 **  Provides functions to measure a temperature using a thermistor
 **  Written By:    Johannes Rebling, 2013
 **
 *********************************************************************************/

//includes---------------------------------------------------------------------
#include <math.h>

//declaration-------------------------------------------------------------------

/*********************************************************************************
 **  measureTemperature()
 **
 **  set speed of a fan using PWM and analog write
 **
 **  Input:  ADC pin
 **  Output: temperature
 *********************************************************************************/
void controlFan(uint_fast8_t fanPin);
