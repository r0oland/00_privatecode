/*********************************************************************************
 **
 **  Provides functions to display my values on the lcd
 **  Written By:    Johannes Rebling, 2013
 **
 *********************************************************************************/

 #ifndef lcd_functions_h
 #define lcd_functions_h

//includes---------------------------------------------------------------------
#include <LiquidCrystal.h>
#include <stdlib.h>

//declaration-------------------------------------------------------------------

 /*********************************************************************************
 **  displayTemperatures
 **
 **  displays the temperatures measured n the LCD Display
 **
 **  Input:  -
 **  Output: -
 *********************************************************************************/
 void display_temperatures();

 /*********************************************************************************
 **  printTemperatureValue
 **
 **  prints the temperature value with limited precision
 **
 **  Input:  thermistor analog pin
 **  Output: -
 *********************************************************************************/
 void print_temperature_value(int analog_Pin);

 /*********************************************************************************
 **  printTemperatureValue()
 **
 **  prints the current and total flow
 **
 **  Input: -
 **  Output: -
 *********************************************************************************/
 void display_flow();

 /*********************************************************************************
 **  displayUptime()
 **
 **  displays the current UpTime
 **
 **  Input: -
 **  Output: -
 *********************************************************************************/
 void display_uptime();

 /*********************************************************************************
 **  timePrinter()
 **
 **  displays time stored in the time array from the modulator function
 **  as d:hh, hh:mm or mm:ss depending on time in array (larges value counts)
 **
 **  Input: displayTime array, array[]=={seconds, seconds/10, minutes/10, minutes, hours}
 **  Output: -
 *********************************************************************************/
 void display_time(unsigned long displayTime[]);

 /*********************************************************************************
 **  modulator()
 **
 **  calculates times that are given in seconds and puts
 **  them into an array in the following maner
 **  array[]=={seconds, seconds/10, minutes/10, minutes, hours}
 **
 **  Input: -
 **  Output: -
 *********************************************************************************/
 void modulator(unsigned long time[]);

 /*********************************************************************************
 **  modulator()
 **
 **  calculates times that are given in seconds and puts
 **  them into an array in the following maner
 **  array[]=={seconds, seconds/10, minutes/10, minutes, hours}
 **
 **  Input:  time_array[], byte time_to_print
 **  Output: -
 *********************************************************************************/
 void print_from_time_array(uint_fast32_t time_array[], uint_fast8_t time_to_print);


 /*********************************************************************************
 **  clearLCD()
 **
 **  clears the lcd, accepts optional periodicClearingTime in which case it's
 **  only cleared every periodicClearingTime seconds
 **
 **  Input: -
 **  Output: -
 *********************************************************************************/
void clear_LCD(uint_fast8_t periodicClearingTime = 0);

 /*********************************************************************************
 **  displayRPM()
 **
 **  display RPM value measured in fan
 **
 **  Input: -
 **  Output: -
 *********************************************************************************/
 void display_RPM();

 #endif
