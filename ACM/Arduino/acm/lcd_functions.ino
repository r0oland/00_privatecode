
//function definitions-------------------------------------------------------------------
void display_temperatures() {
  static uint_fast32_t tempDispTimer;
  if ((millis() - tempDispTimer) > TEMPERATURE_REFRESH_TIME*1000)
  { // only update temperature display every TEMPERATURE_REFRESH_TIME seconds
    lcd.setCursor(0, 0);
    lcd.print("A:");
    print_temperature(THERMISTOR_PIN_0);
    lcd.print(" G:");
    print_temperature(THERMISTOR_PIN_1);
    lcd.print(" S:");
    print_temperature(THERMISTOR_PIN_2);

    tempDispTimer = millis();
  }
}

//---------> printTemperatureValue <------------------------
void print_temperature(int analog_Pin) {
  //char to display temperature with max width of (23.4 degrees)
  char displayString[4];
  //convert to string with given precision
  /*uint_fast16_t bParameter =  B_PARAMETER[0];*/
  float temperature = sample_temperature(analog_Pin,B_PARAMETER[0]);
  // get average value here

  if (temperature > 99.9)
  {
    dtostrf(99.9,4,1,displayString);
  }
  else if (temperature < -99.0)
  {
    dtostrf(-99.0,4,0,displayString);
  }
  else if (temperature < 0.0)
  {
    dtostrf(temperature,4,0,displayString);
  }
  else
  {
    dtostrf(temperature,4,1,displayString);
  }
  lcd.print(displayString);
}

//---------> displayFlow <------------------------
void display_flow() { //displays the following: xxxxxL w x.xxL/min
  static uint_fast32_t flowDisplayTimer;
  static uint_fast16_t lastFlowCount;

  if ((millis() - flowDisplayTimer) > 1000*FLOW_REFRESH_TIME)
  { // only update flow every FLOW_REFRESH_TIME seconds
    float total_flow = totalFlowCount_*COUNTS_TO_LITER;
    char displayString[6]; //max char length = 5 chars

    dtostrf(total_flow,5,0,displayString);//convert to string with precision 2 and store in displayString
    lcd.setCursor(0,1);
    lcd.print(displayString);
    lcd.setCursor(5,1);
    lcd.print("L with ");

    //calc total flow from difference in counts

    uint_fast16_t currentCount = totalFlowCount_-lastFlowCount;
    //flow in L/h
    float currentFlow = currentCount/FLOW_REFRESH_TIME*COUNTS_TO_LITER*60;
    dtostrf(currentFlow,6,2,displayString); //convert to string with given precision
    //display current flow
    lcd.setCursor(6,1);
    lcd.print(" with "); //temp char now contains the current flow
    lcd.setCursor(11,1);
    lcd.print(displayString); //print x.x
    lcd.setCursor(17,1);
    lcd.print("L/h");

    flowDisplayTimer = millis();
    lastFlowCount = totalFlowCount_;
  }
}

//---------> displayUptime <------------------------
void display_uptime() {//display Uptime:
  static uint_fast32_t timer_uptime;
  if ((millis() - timer_uptime) > UPTIME_REFRESH_TIME*1000) //update display every 500ms
  {
    char displayString[6]; //max char length = 5 chars
    uint_fast32_t uptime[] = {
      millis()/1000,0,0,0,0    }; //store current uptime in seconds

    lcd.setCursor(0,3);
    lcd.print("Uptime: ");
    lcd.setCursor(8,3);
    display_time(uptime);

    timer_uptime = millis();
  }
}


//---------> TimePrinter <------------------------
void display_time(uint_fast32_t timeArray[]){
  modulator(timeArray); //{total sec,0,0,0,0} -> {total sec,d,h,min,s}

  if (timeArray[1] > 0) //display time 01d44h
  {
    print_from_time_array(timeArray,1);
    print_from_time_array(timeArray,2);
  }
  else if (timeArray[2] > 0) //display time 12h44m
  {
    print_from_time_array(timeArray,2);
    print_from_time_array(timeArray,3);
  }
  else  //display time 02m22s
  {
    print_from_time_array(timeArray,3);
    print_from_time_array(timeArray,4);
  }
}

//---------> Modulator <------------------------
// takes a pointer to a time array of the format
// timeArray[] = {totalSeconds,0,0,0,0} and transforms it into
// timeArray[] = {totalSeconds,d,h,min,s}
void modulator(uint_fast32_t timeArray[]){
  uint_fast32_t seconds[] = {
    86400, 3600, 60    };

  for (int i=1; i<=4; i++)
  {
    timeArray[i] = timeArray[i-1]%seconds[i-1];
  }

  for (int n=3; n>0; n--)
  {
    timeArray[n] = timeArray[n-1]/seconds[n-1];
  }
}

void print_from_time_array(uint_fast32_t time_array[], uint_fast8_t time_to_print){
  //added ~ to make entries aggree between time_array[] and time_units[]
  char time_units[] = {"~dhms"};
  // char time_units[] = {"0dhms"};
  //i.e. time_array[1] corresponds to days, so time_units[1] should give "d"
  //print second digit before coma, i.e. the 1 in 12
  lcd.print(time_array[time_to_print]/10);
  //print first digit before coma, i.e. the 2 in 12
  lcd.print(time_array[time_to_print]%10);
  //print correct unit (i.e. d,h,m or s)
  lcd.print(time_units[time_to_print]);
}

//---------> clearLCD <------------------------
void clear_LCD(uint_fast8_t periodicClearingTime){
  static uint_fast32_t timer;
  if ((millis() - timer) > (1000*periodicClearingTime)) //update display every 30s
  {
    lcd.clear();
    timer = millis();
  }
}

//---------> displayRPM <------------------------
void display_RPM(){
  static uint_fast32_t timer;

  if ((millis() - timer) > (1000*RPM_REFRESH_TIME)) //update every 5s cause value varies a lot...
  {
    lcd.setCursor(0,2);
    lcd.print("RPM: ");

    lcd.print(totalRpmCount_*60/RPM_REFRESH_TIME/2);//assuming two signals per revolution

    timer = millis();
    totalRpmCount_ = 0;
  }
}
