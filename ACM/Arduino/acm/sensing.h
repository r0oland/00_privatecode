#ifndef sensing_h
#define sensing_h

#include <math.h>

struct  Thermistor
{
  uint_fast16_t MaxTemperature;
  uint_fast16_t MinTemperature;
  uint_fast16_t MeanTemperature;
  uint_fast16_t CurrenTemperature;

  uint_fast8_t SensorPin; //Analog pin of the sensor
  uint_fast8_t BParameter; //Type of sensor defined by bParameter
  uint_fast16_t RefResist = 10000; //Resistance of Reference resistor
  uint_fast16_t NomResist = 10000; //Nominal Resitance of Therm at 25deg C
};

/*********************************************************************************
 **  measureTemperature()
 **
 **  Function calculates temperature based on value measured via analog pin.
 **
 **  Input:  ADC pin
 **  Output: temperature
 *********************************************************************************/
float measureTemperature(int analogPin);


/*******************************************************************************
 **  sample_temperature
 **
 **  read temperature using thermistor in voltage devider
 **
 **  Input:  analog pin, bParameter from Steinh-Hard Equation
 **  Output: float temperaure
 *********************************************************************************/
float sample_temperature(uint_least8_t analogPin, uint_fast16_t bParameter);


/*********************************************************************************
 **  increaseFlowCounter
 **
 **  Function is called when interrupt is triggered on digital pin 2 by a rising
 **  signal and increase the flow counter
 **
 **  Input:  None
 **  Output: None
 *********************************************************************************/
void increaseFlowCounter();

/*********************************************************************************
 **  increaseRPMCounter
 **
 **  Function is called when interrupt is triggered on digital pin 3 by a rising
 **  signal and increase the fan RPM counter
 **
 **  Input:  None
 **  Output: None
 *********************************************************************************/
void increaseRPMCounter();

#endif
