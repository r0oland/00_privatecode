//measureTemperature------------------------------------------------------------
void measure_temperature(){
  // measure temp for all sensors
  for (uint_fast8_t iSensor=0; iSensor<= nSensors; iSensor++)
  {

  }
}

float update_average_temperature(int analogPin) {
  static uint_fast32_t timer_temp_average;
  if ((millis() - timer_temp_average) > TEMPERATURE_REFRESH_TIME*1000) //update display every 500ms
  {
    lcd.setCursor(0, 0);
    lcd.print("A:");
    print_temperature(THERMISTOR_PIN_0);
    lcd.print(" G:");
    print_temperature(THERMISTOR_PIN_1);
    lcd.print(" S:");
    print_temperature(THERMISTOR_PIN_2);

    timer_temp_average = millis();
  }
  /*sample_temperature(analogPin,bParameter);*/

}

//sample_temperature-----------------------------------------------------------
float sample_temperature(uint_least8_t analogPin, uint_fast16_t bParameter) {
  float resistance;
  float temperature;
  float rInf = (float)THERM_RESIST*exp(-(float)bParameter/AMBIENT_TEMP);

  resistance = (float)REF_RESIST*1023.0/analogRead(analogPin) - (float)REF_RESIST;
  temperature = (float)bParameter/log(resistance/rInf);
  temperature = temperature - 273.15;  // Convert Kelvin to Celsius
  return temperature;
}

//increaseFlowCounter-----------------------------------------------------------
void increaseFlowCounter(){
  totalFlowCount_++;
}

//increaseRPMCounter------------------------------------------------------------
void increaseRPMCounter(){
  totalRpmCount_++;
}
