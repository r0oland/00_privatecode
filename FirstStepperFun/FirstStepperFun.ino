/*
 * MotorKnob
 *
 * A stepper motor follows the turns of a potentiometer
 * (or other sensor) on analog input 0.
 *
 * http://www.arduino.cc/en/Reference/Stepper
 * This example code is in the public domain.
 */

#include <Stepper.h>

// change this to the number of steps on your motor
#define STEPS 360/7.5

// create an instance of the stepper class, specifying
// the number of steps of the motor and the pins it's
// attached to
Stepper stepper(STEPS, 50, 51, 52, 53);

// the previous reading from the analog input
int previous = 0;
int currentAngle = 0;
int stepsRequired = 0;

void setup()
{
  // set the speed of the motor to 30 RPMs
  stepper.setSpeed(30);
}

void loop()
{
  // // get the sensor value
  // int setAngle = (analogRead(0)/1023)*360;
  // stepsRequired = (setAngle-currentAngle)/7.5;
  // currentAngle = setAngle;
  // // move a number of steps equal to the change in the
  // sensor reading
    // delay(1000);
    for(int i=0; i<50; i++){
        stepper.step(i);
    }
}
