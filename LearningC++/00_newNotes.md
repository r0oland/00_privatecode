### Preprocessor - #include and such
- used much less in C++ than it was in C, example would be
```cpp
#include
```

#### System and User Headers
- there are tow main types of header files:
  - system headers which are know to the compiler such as
  ```cpp
  #include <iostream>
  ```
  - user header which are not which are looked for in the current subdirectory, not the system path, such as
  ```cpp
  #include "user_types.h"
  ```
- header files are directly placed where they are included, hence
  - only include what is really needed to avoid long compiling
  - headers **MUST NOT** contain definitions but only declarations, otherwise there could be duplicate definitions

#### More Complex Preprocessor Fun
  - if-like structure for preprocessor
    ```cpp
    #if constant
      true-code
    #else //optional
      false-code
    #endif
    ```
  - check for alreay existing definitions
    ```cpp
    #ifdef identifier // is defined
      true-code
    #else
      false-code
    #endif
    // or alternative
    #ifndef identier
      true-code
    #else
      false-code
    #endif
    ```
  - avoid double-inclusions using:
  ```cpp
  #ifndef filename_h___
  #define filename_h___
      //... complete code of header ...
  #endif
  // e.g. for a header file called user_types.h
  #ifndef user_types_h___
  #define user_types_h___
      // ... complete code of header ...
  #endif
  ```

## Object Oriented Programming

### Classes and Objects - General Concepts
- classes summarize and describe natural properties of something, i.e. a human has a head, torso, legs
- Objects are concrete instance of classes, they have a specific status and specific properties which are given by it's corresponding class
- a class therefore defines a datatype, an object describes a variable
- ** Inheritance **
  - a base-class defines the minimal functionality and properties that an object of that class must have or must be able to do
  - form this base-class so called sub-classes can be derived, called inheriting
  - these sub-classes can add properties as well as change existing once, however they can and should not remove properties from their parent classes
  - sub-classes can inherit their properties from other sub-classes, i.e. they don't always have to inherit from a base class
  - classes can be derive from a single class (single inheritance) or from multiple classes (multiple inheritance)
- Classes summary
  - **Member-Variables:** data an object hold is stored in member variables which can't be directly accessed from the outside
  - **Class-Member-Variables:** variables which are shared by the classes member, can be for example useful to know how many class members there are
  - **Methods:** are basically the functions which are declared to be used with that class, for example to access its member variables or to actually do things
  - **Class-Methods:** are methods which are independent of the specifc member and can hence only access class member variables
- ** the inheritance of a class has IS-A relation ** i.e. a book author class can inherit properties from a human class, an author IS-A human
- ** use member-variables for HAS-A relations ** i.e. a human HAS-A head and hence the head is a member-variable

### Structures in C++
- see struct_meht_demo.cpp
- pretty self-explaining, not too much new, structures in C++ are just classes with all members being public

### Classes in C++
- see memory_game_card.h for a proper class declaration and explanation therein

#### Access Speciers:
- **private:** only methods of the class itself as well as friend functions/classes can access this variables. It is not possible to change the content of these members from the outside or from sub-classes
- **protected:** methods of the class, friend functions as well as methods of sub-classes can access these members
- **public:** free for all!
