#include "memory_game_card.h"
#include <cstring>

//------------------------------------------------------------------------------
// constructor of MemoryGameCard

MemoryGameCard::MemoryGameCard(const char *symbol)
{
  if(!symbol)
  {
    Symbol = 0;
    return;
  }
  Symbol = new char[strlen(symbol) +1];
  strcpy(Symbol,symbol);
}

// destructor of MemoryGameCard
MemoryGameCard::~MemoryGameCard()
{
  if(Symbol)
    delete[] Symbol;
}

const char *MemoryGameCard::Get_Symbol()
{
  return(Symbol);
}

void MemoryGameCard::Change_Symbol(const char *symbol)
{
  if(Symbol)
    delete[] Symbol;

  if(!symbol)
  {
    Symbol = 0;
    return;
  }

  Symbol = new char[strlen(symbol) + 1];
  strcpy(Symbol, symbol);
}
