#ifndef memory_game_card_h___
#define memory_game_card_h___

// 􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀MemoryGameCard declaration file as it should be!
// model of a card as used in the game "memory"

class MemoryGameCard
{
protected: // no access from the outside but can be changed by friends and subclasses
  char *Symbol;
public: // free for all
  // constructor, gets automatically called when an instance of this class is created
  MemoryGameCard(const char *symbol);
  ~MemoryGameCard(); //destructor, automatically called when lifetime ends!

  const char *Get_Symbol();
  void Change_Symbol(const char *symbol);
};

#endif
