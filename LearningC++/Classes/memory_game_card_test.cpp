#include <iostream>
#include "memory_game_card.h"

using std::cout;
using std::endl;

// ------------
int main(int32_t argc, char *argv[])
{
  MemoryGameCard testCard("A");

  cout << testCard.Get_Symbol() << endl;

  testCard.Change_Symbol("B");

  cout << testCard.Get_Symbol() << endl;

  return(0);
}
