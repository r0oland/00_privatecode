#include <cstring>
#include <cstdint>

#include "memory_game_card_v2.h"

uint16_t MemoryGameCard::NInstances = 0;
//------------------------------------------------------------------------------
// constructor of MemoryGameCard

MemoryGameCard::MemoryGameCard(const char *symbol)
{
  NInstances++;
  if(!symbol)
  {
    Symbol = 0;
    return;
  }
  Symbol = new char[strlen(symbol) +1];
  strcpy(Symbol,symbol);
}

// destructor of MemoryGameCard
MemoryGameCard::~MemoryGameCard()
{
  if(Symbol)
    delete[] Symbol;

  NInstances++;
}

const char *MemoryGameCard::Get_Symbol()
{
  return(Symbol);
}

void MemoryGameCard::Change_Symbol(const char *symbol)
{
  if(Symbol)
    delete[] Symbol;

  if(!symbol)
  {
    Symbol = 0;
    return;
  }

  Symbol = new char[strlen(symbol) + 1];
  strcpy(Symbol, symbol);
}

uint16_t MemoryGameCard::Get_N_Instances()
{
  return(NInstances);
}
