#ifndef memory_game_card_v2_h___
#define memory_game_card_v2_h___

// 􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀MemoryGameCard declaration file as it should be!
// model of a card as used in the game "memory"

class MemoryGameCard
{
protected:
  static uint16_t NInstances; // static turns nInstances into a clas member
  char *Symbol;
public:
  MemoryGameCard(const char *symbol);
  ~MemoryGameCard();

  const char *Get_Symbol();
  void Change_Symbol(const char *symbol);
  static uint16_t Get_N_Instances(); // makes it a class-member-method
};

#endif
