#include <iostream>
#include <cstdint>

#include "memory_game_card_v2.h"

using std::cout;
using std::endl;

// ------------
int main(int32_t argc, char *argv[])
{
  MemoryGameCard testCard("A");

  cout << MemoryGameCard::Get_N_Instances() << endl;

  MemoryGameCard testCard2("b");

  cout << MemoryGameCard::Get_N_Instances() << endl;
  cout << testCard.Get_N_Instances() << endl;
  cout << testCard2.Get_N_Instances() << endl;

  return(0);
}
