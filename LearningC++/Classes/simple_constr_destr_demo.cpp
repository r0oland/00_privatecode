#include <iostream>

using std::cout;
using std::endl;

class SimpleConstrDestrDemo
{
private:
  int justAnIntVariable;
  const char *stringValue;
public:
  SimpleConstrDestrDemo();
  SimpleConstrDestrDemo(int aParameter);
  SimpleConstrDestrDemo(const char *aParameter);
  ~SimpleConstrDestrDemo();
};

SimpleConstrDestrDemo::SimpleConstrDestrDemo()
{
  cout << "default constructor " << endl;
  justAnIntVariable = 0;
  stringValue = "default constructed";
}


SimpleConstrDestrDemo::SimpleConstrDestrDemo(int aParameter)
{
  cout << "first non default constructor " << endl;
  justAnIntVariable = aParameter;
  stringValue = "default constructed";
}


SimpleConstrDestrDemo::SimpleConstrDestrDemo(const char *aParameter)
{
  cout << "second non default constructor " << endl;
  justAnIntVariable = 0;
  stringValue = aParameter;
}

SimpleConstrDestrDemo::~SimpleConstrDestrDemo()
{
  cout << "destructor " << endl;
  cout << justAnIntVariable << " " << stringValue << endl;
}

int main(int argc, char *argv[])
{
  SimpleConstrDestrDemo testVar1;
  SimpleConstrDestrDemo testVar2(12);
  SimpleConstrDestrDemo testVar3("abc");

  cout << "all init" << endl;


  return(0);
}
