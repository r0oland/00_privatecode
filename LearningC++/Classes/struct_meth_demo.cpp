#include <iostream>
#include <cstring>

using std::cout;
using std::endl;

struct Name
{
  char *FirstName; // member variables, i.e. capitalized
  char *LastName;

  void Init(const char *initFirstName, const char *initLastName);
  void Set_First_Name(const char *setFirstName);
  void Set_Last_Name(const char *setLastName);
  const char *Get_First_Name();
  const char *Get_Last_Name();
}; //<-------- don't forget this one!!!

void Name::Init(const char *initFirstName, const char *initLastName)
{
  if (!initFirstName) // initFirstName is a standard auto variable
    FirstName = 0;
  else
  {
    FirstName = new char[strlen(initFirstName) + 1];
    strcpy(FirstName, initFirstName);
  }

  if (!initLastName)
    LastName = 0;
  else
  {
    LastName = new char[strlen(initLastName) + 1];
    strcpy(LastName, initLastName);
  }
}

void Name::Set_First_Name(const char *setFirstName)
{
  if (FirstName)
    delete[] FirstName;
  if(!setFirstName)
    FirstName = 0;
  else
  {
    FirstName = new char[strlen(setFirstName) + 1];
    strcpy(FirstName, setFirstName);
  }
}

void Name::Set_Last_Name(const char *setLastName)
{
  if (LastName)
    delete[] LastName;
  if(!setLastName)
    LastName = 0;
  else
  {
    LastName = new char[strlen(setLastName) + 1];
    strcpy(LastName, setLastName);
  }
}

const char *Name::Get_First_Name()
{
  return(FirstName);
}

const char *Name::Get_Last_Name()
{
  return(LastName);
}

// ------------
int main(int32_t argc, char *argv[])
{
  Name oneName;
  oneName.Init("John", "Doe");

  Name anotherName;
  anotherName.Init("Joe", "Smith");

  cout << oneName.Get_First_Name() << " " << oneName.Get_Last_Name() << endl;
  cout << anotherName.Get_First_Name() << " " << anotherName.Get_Last_Name() << endl;

  oneName.Set_First_Name("Mark");
  oneName.Set_Last_Name("McDonalds");

  cout << oneName.Get_First_Name() << " " << oneName.Get_Last_Name() << endl;
  cout << anotherName.Get_First_Name() << " " << anotherName.Get_Last_Name() << endl;

  return(0);
}
