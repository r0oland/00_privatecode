//demo program to show the behaviour of C++ Variables

#include <iostream> // ohne .h!!
#include <cstdint>

using std::cout;
using std::endl;

uint_fast16_t firstGlobalArray_[5]; //implicit init

uint_fast16_t secondGlobalArray_[] = {1, 2, 5, 23, 42}; //explicitly init

uint_fast16_t thridGlobalArray_[5] = {1, 2, 5}; // partially init,the rest implicit

int main(int argc, char *argv[])
{
  uint_least8_t count;

  cout << "implicit init array ";
  for (count = 0; count < 5 ; count++)
    cout << firstGlobalArray_[count] << " ";
  cout << endl;

  cout << "explicit init array ";
  for (count = 0; count < 5 ; count++)
    cout << secondGlobalArray_[count] << " ";
  cout << endl;

  cout << "partially init array ";
  for (count = 0; count < 5 ; count++)
    cout << thridGlobalArray_[count] << " ";
  cout << endl;

  return(0);
}
