#include <iostream>

using std::cout;
using std::endl;

// dunc deklaration
void show(int32_t num, bool showPrefix = false);

// ------------
int main(int argc, char *argv[])
{
  show(17);
  show(17,false);
  show(17,true);
}

void show(int32_t num, bool showPrefix)
{
  if (showPrefix)
  {
    cout << "pref on biatch!";
  }
  cout << num << endl;
}
