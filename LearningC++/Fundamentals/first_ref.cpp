#include <iostream>

using std::cout;
using std::endl;

// dunc deklaration
void change_var(int32_t &var);
// ------------
int main(int32_t argc, char *argv[])
{
  int32_t myVar = 12;
  int32_t &myRef = myVar;

  cout << " myVar: " << myVar << " myRef" << myRef << endl;

  myVar *= 2;
  cout << " myVar: " << myVar << " myRef" << myRef << endl;

  myRef *= 2;
  cout << " myVar: " << myVar << " myRef" << myRef << endl;

  change_var(myVar);
  cout << " myVar: " << myVar << " myRef" << myRef << endl;

  change_var(myRef);
  cout << " myVar: " << myVar << " myRef" << myRef << endl;

  return(0);
}

void change_var(int32_t &var)
{
  var /= 2;
}
