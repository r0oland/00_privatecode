#include <iostream>

using std::cout;
using std::endl;

// dunc deklaration
int32_t square(int32_t num);
double square(double num);
void show(int32_t num);
void show(double num);

// ------------
int main(int argc, char *argv[])
{
  int32_t aNumber = 4;
  double oneMoreNumber = 4.0;

  int32_t aSquare = square(aNumber);
  double oneMoreSquare = square(oneMoreNumber);

  show(aSquare);
  show(oneMoreSquare);

  int16_t andOneMore = 3;
  show(square(andOneMore));
}

int32_t square(int32_t num)
{
  return(num*num);
}

double square(double num)
{
  return(num*num);
}

void show(int32_t num)
{
  cout << "showing off numbers: " << num << endl;
}

void show(double num)
{
  cout << "showing off doubles: " << num << endl;
}
