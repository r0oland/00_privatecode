//demo program to show the behaviour of C++ Variables

#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t

using std::cout;
using std::endl;

int_least16_t firstDemoMatrix_[3][3]; //implicit init
int_least16_t secondDemoMatrix_[][3] = {{1,2,3},{4,5,6},{7,8,9}};
int_least16_t thirdDemoMatrix_[3][3] = {{1,2,3},{4,5}};
int_least16_t fourthDemoMatrix_[3][3] = {{1,2,3},{4,5},{6,7,8}};
int_least16_t fifthDemoMatrix_[3][3] = {1,2,3,4,5,6,7,8};


int main(int argc, char *argv[])
{

  cout << "implicit:" << endl;
  for (uint_fast8_t outer_count = 0; outer_count < 3; outer_count++) {
    for (uint_fast8_t inner_count = 0; inner_count < 3; inner_count++) {
      cout << firstDemoMatrix_[outer_count][inner_count] << " ";
    }
    cout << endl;
  }

  return(0);
}
