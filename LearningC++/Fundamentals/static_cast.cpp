#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t
#include <typeinfo>

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
  int32_t anIntVar = 17;
  float convFloatVar = static_cast<float>(anIntVar);

  cout << "convFloatVar " << convFloatVar << endl;

  convFloatVar = 13.45;

  cout << "convFloatVar " << (uint32_t)convFloatVar << endl;

  convFloatVar = 13.999;
  anIntVar = static_cast<int32_t>(convFloatVar);

  cout << "conv int var " << anIntVar << endl;

  anIntVar = -1;

  cout << static_cast<uint32_t>(anIntVar);

  return(0);

}
