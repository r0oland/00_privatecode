#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t

using std::cout;
using std::endl;

struct Pixel // structure declaration
{
  uint_least16_t XCoord;
  uint_least16_t YCoord;
  uint_least16_t RGBValue[3];
};

struct Pixel GlobalPixel_; //implicit init

int main(int argc, char *argv[])
{
  struct Pixel OnePixel;
  OnePixel.XCoord = 10;
  OnePixel.YCoord = 30;
  OnePixel.RGBValue[0] = 0xFF; // max red
  OnePixel.RGBValue[1] = 0x00; // no green
  OnePixel.RGBValue[2] = 0x00; // no blue

  cout << "explicit init: " << endl << "(" << GlobalPixel_.XCoord <<
  ", " << GlobalPixel_.YCoord << "), with RGB: " << OnePixel.RGBValue[0]  << '|'  <<  OnePixel.RGBValue[1] << '|' << OnePixel.RGBValue[2]<< endl;
  return(0);
}
