#include <iostream>

using std::cout;
using std::endl;

struct TestStruct
{
  int32_t aMember;
  int32_t anoMemmber;
};

void show(struct TestStruct &TheStruct);
void change_struct(struct TestStruct &TheStruct);

// ------------
int main(int32_t argc, char *argv[])
{
  struct TestStruct MyStruct;

  MyStruct.aMember = 17;
  MyStruct.anoMemmber = 20;

  show(MyStruct);
  change_struct(MyStruct);
  show(MyStruct);

  return(0);
}

void change_struct(struct TestStruct &TheStruct)
{
  TheStruct.aMember = 55;
  TheStruct.anoMemmber = 55;
}
void show(struct TestStruct &TheStruct)
{
  cout << TheStruct.aMember << TheStruct.anoMemmber << endl;
}
