#include <stdio.h>
#include <stdlib.h>

int main()
{
  int aNumber = 0;
  unsigned count = 0;
  unsigned char *help = NULL;
  //&aNumber would give and int* , i.e. an pointer to the address of an int
  // but we want to check int byte by byte, i.e. we want a (unsigned char*)
  // we end up with an unsigned char*, pointing to the byte right next to
  // last byte of the int value (i.e. doesn't belong to us!)
  // help = ((unsigned char*)&aNumber) + sizeof(int);
  help = ((unsigned char*)&aNumber); //now a bit pointer to begin of int
  help += sizeof(int); // move to the right of the last byte in int

  count = sizeof(int);
  while(--count)
    *--help = (unsigned char)(count | 0xf0);

  printf("%x\n", aNumber);
  return 0;
}
