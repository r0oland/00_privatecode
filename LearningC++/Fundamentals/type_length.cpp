#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t

using std::cout;
using std::endl;

int main(int argc, char *argv[])
{
  cout << "int_fast8_t: " << sizeof(int_fast8_t)*8 << endl;
  cout << "int_fast16_t: " << sizeof(int_fast16_t)*8 << endl;
  cout << "int_fast32_t: " << sizeof(int_fast32_t)*8 << endl;
  cout << "int_fast64_t: " << sizeof(int_fast64_t)*8 << endl;

  cout << "int_least8_t: " << sizeof(int_least8_t)*8 << endl;
  cout << "int_least16_t: " << sizeof(int_least16_t)*8 << endl;
  cout << "int_least32_t: " << sizeof(int_least32_t)*8 << endl;
  cout << "int_least64_t: " << sizeof(int_least64_t)*8 << endl;
  return(0);
}
