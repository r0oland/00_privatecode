#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t
#include <typeinfo>

using std::cout;
using std::endl;

typedef struct _TestStruct_ // structure declaration
{
  int16_t JustAMember;
  int8_t OneMoreMember;
} TestStruct;

typedef union _TestUnion_
{
  int16_t JustAMember;
  int8_t OneMoreMember;
} TestUnion;

int main(int argc, char *argv[])
{
  int32_t testVar = 0;

  cout << typeid(char).name()<< endl;
  cout << typeid(signed char).name()<< endl;
  cout << typeid(unsigned char).name()<< endl;
  cout << typeid(short).name()<< endl;
  cout << typeid(signed short).name()<< endl;
  cout << typeid(int).name()<< endl;
  cout << typeid(int32_t).name()<< endl;

  cout << typeid(_TestStruct_).name()<< endl;
  cout << typeid(TestStruct).name()<< endl;
  cout << typeid(_TestUnion_).name()<< endl;
  cout << typeid(TestUnion).name()<< endl;
}
