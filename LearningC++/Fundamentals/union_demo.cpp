#include <iostream> // ohne .h!!
#include <cstdint> // for uint_least8_t

using std::cout;
using std::endl;

const uint_least8_t KEY_EVENT   1;
const uint_least8_t MOUSE_EVENT 2;

struct KeyEvent // structure declaration
{
  uint_least16_t EventType;
  uint_least8_t Key;
  uint_least8_t Modifiers;
};

struct MouseEvent
{
  uint_least16_t EventType;
  uint_least16_t XCoord;
  uint_least16_t YCoord;
  uint_least8_t ButtonsPressed;
};

union Event
{
  uint_least16_t EventType;
  struct KeyEvent AnKeyEvent;
  struct MouseEvent AMouseEvent;
};

int main(int argc, char *argv[])
{
  union Event AnEvent;

  AnEvent.EventType = KEY_EVENT;
  AnEvent.AnKeyEvent.Key = 'x';
  AnEvent.AnKeyEvent.Modifiers = 0;

  AnEvent.EventType = MOUSE_EVENT;
  AnEvent.AMouseEvent.XCoord = 10;
  AnEvent.AMouseEvent.YCoord = 40;
  AnEvent.AMouseEvent.ButtonsPressed = 0;

  return(0);
}
