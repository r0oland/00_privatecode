//demo program to show the behaviour of C++ Variables

#include <iostream> // ohne .h!!

using std::cout;
using std::endl;

void a_function();

int aGlobalVar_ = 17;
int anotherGlobalVar_;

int main(int argc, char *argv[])
{
  a_function();
  cout << "aGlobalVar_" << aGlobalVar_ << endl;
  cout << "anotherGlobalVar_" << anotherGlobalVar_ << endl;

  bool aBooleanVar = true;
  cout << "bool set to true: " << aBooleanVar << endl;
  aBooleanVar = false;
  cout << "bool set to false: " << aBooleanVar << endl;
  aBooleanVar = 23;
  cout << "bool set to 23: " << aBooleanVar << endl;

  int anIntergerVar = aBooleanVar;
  cout << "bool to int: " << anIntergerVar << endl;

  char testChar = 255;
  int testInt = testChar;
  cout << "signed(-1) or unsigned (255) char? " << testInt << endl;

  a_function();

  return(0);
}

void a_function()
{
  static int callCounter; // implicitly init to 0

  cout << "Call counter: " << ++callCounter << endl;

  int unIntAutoVar;
  cout << "random crap: " << unIntAutoVar << endl;
}
