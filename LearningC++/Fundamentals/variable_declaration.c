#include<stdio.h>

main()
{
  //============================================================================
  // external variables ========================================================
  //============================================================================
  // an external variable is a variable defined outside any function block
  /* to get variables accross different files:
  - define varialbe in ONE C file
  - deklare variable as extern in ONE header file
  - all files that need the varialbe need to include that header file!
  e.g:
  my_definitions.c -> int my_global_;
  my_definitions.h -> extern int my_global_;
  my_program.c -> #include "my_definitions.h"
  */

  //============================================================================
  // register variables ========================================================
  //============================================================================
  // Registers are faster than memory to access, so the variables which are most frequently used in a C program can be put in registers using register keyword. The keyword register hints to compiler that a given variable can be put in a register. It’s compiler’s choice to put it in a register or not. Generally, compilers themselves do optimizations and put the variables in register

  //============================================================================
  // static variables ========================================================
  //============================================================================
  // a static variable is a variable that has been allocated statically—whose lifetime or "extent" extends across the entire run of the program
}
