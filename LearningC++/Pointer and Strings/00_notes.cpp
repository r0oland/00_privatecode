// conditional expression
condition ? true-expr : false-expr;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Referencing
// see struct_ref_clean.cpp
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// creates pointer to memory location, allows alternative access to value
// stored in that memory location
// allows simple and more or less safe call-by-reference in order
// to change content of a variable in a function
// also great for performance when using structs as not complete struct
// need to be copied to be used in function but only ref value needed
int32_t myVar = 12;
int32_t &myRef = myVar; // needs to be explicitly init when defined!

change_var(myRef); // will work and won't cause double de-ref
change_var(myVar); // will also work and will also change the variable content!

void change_var(int32_t &var)
{
  var /= 2;
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Pointer
// see first_pointer_try.cpp for very simple example based on C
// see first_pointer_demo.cpp for very simple example based on C++
// see call_by_ref.cpp for call by reference example
// see pointing_pointer.cpp for pointer on pointer fun!
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/* Pointer are just variables that contain an address, i.e. they point to a variable! They are simple which nakes them both powerfull and dangerous. They allow dynamic memory allocation, which is essential for most real world programs (see next section).
If we want to access the content of a variable that a pointer points to, we need to de-reference the pointer, i.e. resolve the address stored in the pointer.
*/

// Quick Pointer Notes:
// - contain START address, nothing more, nothing less (i.e. know your variable type or go home!)
// - dereferencing = resolving the address to access actual content stored at the variable address the pointer references
// - use * (dereference operator) on pointer, i.e. the address, to get value stored at that address
// - use & (dereference operator) on variable to get address of that variable
// - deref operator also used in variable definition/deklaration to make it a pointer
// - with pointers, you don't have to worry about the scope of your variable, but you have to worry about the LIFETIME!

int16_t myVar = 17; // normal variable
int16_t *myPointer = & myVar; // creates pointer to myVar using address-of operature "&" to get address and dereference operator "*" to make it a pointer
*myPointer = 55; // change content stored at address of pointer
myVar = 55; // does the exact same thing as the line above!
*notUsedPtr = 0; //was NULL for C, but C++ 0 is saver!

change_var(&myVar);// call function and hand over a pointer
// define function which expects a pointer (deref operator used in variable definition makes that variable a pointer) to a variable
void change_var(int16_t *myVar)
{
  *myVar = 77;
}

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Dynamic memory allocation using Pointer - C+++ Style
// using new, delete and delete[]
// see first_pointer_demo.cpp for super simple example
// see first_dynamic_memory_demo.cpp for memory allocation and freeing
// see ptr_cal_by_ref.cpp for references to a pointer, i.e. call-by-ref
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// allocate new memory using new, no need for sizeof anymore!
int32_t *dynVar = new int32_t; // get base pointer to 4 byte (32bit) variable
// change content of variable stored at base ptr adress, don't forget the *
*dynVar = 10;
// clean up, but dynVar is still a pointer, i.e. can be used again if wanted
delete dynVar; // clean (free up) memory
dynVar = 0; // ALWAYS do this to make sure using this ptr will fail!
dynVar = new int32_t(5); // get ptr to memory block and init that block to 5
// also works with arrays:
const uint32_t ARRAY_LENGTH = 10;
int32_t *dynArray = new int32_t[ARRAY_LENGTH];
*iterationPtr = dynArray;
iterationPtr[5]
// is the same as
*(iterationPtr + 5) //whatch out for the Brackets!!!!!
// where both access the 5th element
delete[] dynArray; // !!!! don't forget [] for arrays!
// reference to a pointer
int32_t *&myPtr; // Reference auf einen Pointer auf einen int32

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Dynamic memory allocation using Pointer - C Style using malloc and free
// see dyn_array.cpp
// see pointer_math.cpp
// Unless you know exactly what you are doing, DO NOT USE malloc, realloc and
// free in C++!
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// allocate memory block:
void *malloc(size_t size); // i.e. used as:
*myBlock = malloc(30*sizeof(int));

// change size:
void *realloc(void *base_ptr,size_t size); //i.e. used as:
*myNewBlock = realloc(myBlock,15*sizeof(int));

// free memory:
void free(void *base_ptr); // i.e. used as:
void free(myBlock);

/* allocate memory using *malloc(size_t size) with size in Bytes!!!
- e.g. to be able to store 30 int values one needs:
  *myBlock = malloc(30*sizeof(int));
  after which myBlock is a pointer to the newly allocated memory
- allocated memory is NOT initialized, i.e. filled with rand values
- ALWAYS use sizeof instead of assuimg a size!
- the returned pointer points to the base-pointer of the memory block and MUST NOT be lost, otherwise one can't make any changes (resize, delete,...) the block

- reallocationg memory using void *realloc(void *base_ptr,size_t size);
- if memory size increases, all stuff still stored as before and new part not initialized
- if memory size decreases, all stuff still stored where it was before except the part that is now troughn away
- there is no way of getting the size of a memory block after the reallocation...you have to know your size!

- get rid of allocated memory using
  void free(myBlock);
  where myBlock must be pointer given by malloc!
- good practice to set pointer to NULL after freeing it to make sure that still using it will cause a segment violation and hence a "stable" crash


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Strings in C
// see string.cpp
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
/* strings are basically a char* (char pointer) with the last character in the string being a '\0'


// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Preprocessor -
// #include and such
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
- used much less in C++ 
