#include <iostream>

using std::cout;
using std::endl;

void change_var(int16_t *myVar);

// ------------
int main(int32_t argc, char *argv[])
{
  int16_t myVar = 17;

  cout << myVar << endl;
  change_var(&myVar);
  cout << myVar << endl;

  return(0);
}

void change_var(int16_t *myVar)
{
  *myVar = 77;
}
