#include <iostream>
#include <stdlib.h>     /* malloc, free, rand */

using std::cout;
using std::endl;

#define INIT_ELE 5
#define REAL_ELE 10

// ------------
int main(int32_t argc, char *argv[])
{
  int16_t *testArray = NULL;
  testArray = static_cast<int16_t*>(malloc(INIT_ELE*sizeof(int16_t)));
  int16_t count = 0;

  while (count < INIT_ELE)
    testArray[count++] = 2*count;

  for (count = 0; count < INIT_ELE; count++)
    cout << testArray[count] << " ";
  cout << endl;

  testArray=static_cast<int16_t*>(realloc(testArray,REAL_ELE*sizeof(int16_t)));

  cout << "After Realloc, no init:" << endl;
  for (count = 0; count < REAL_ELE; count++)
    cout << testArray[count] << " ";
  cout << endl;

  for (count = INIT_ELE; count < REAL_ELE; count++)
    testArray[count] = count*2;

  cout << "After Realloc:" << endl;
  for (count = 0; count < REAL_ELE; count++)
  cout << testArray[count] << " ";

  free(testArray);
  testArray = NULL;

  return(0);
}
