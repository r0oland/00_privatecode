#include <iostream>
#include <stdlib.h>     /* malloc, free, rand */

using std::cout;
using std::endl;

void show(const int32_t array[], uint32_t length);

// ------------
int main(int32_t argc, char *argv[])
{
  int32_t *dynVar = new int32_t;
  *dynVar = 10;
  cout << *dynVar << endl;
  delete dynVar;

  dynVar = new int32_t(5);
  cout << *dynVar << endl;
  delete dynVar;

  dynVar = 0;

  const uint32_t ARRAY_LENGTH = 10;

  int32_t *dynArray = new int32_t[ARRAY_LENGTH];

  for (uint32_t count=0; count < ARRAY_LENGTH; count++)
    dynArray[count] = count;

  show(dynArray, ARRAY_LENGTH);

  int32_t *iterationPtr = dynArray;
  cout << iterationPtr[5] << " " << *(iterationPtr + 5) << endl;

  delete[] dynArray;
  dynArray = 0;

  return(0);
}

// since ptr = array this works with pointers as well!
void show(const int32_t array[], uint32_t length)
{
    while(length--)
      cout << "[" << *array++ << "]";
    cout << endl;
}
