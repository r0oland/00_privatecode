#include <iostream>
#include <stdlib.h>     /* malloc, free, rand */

using std::cout;
using std::endl;

void change_var(int32_t *var);

// ------------
int main(int32_t argc, char *argv[])
{
  int32_t myVar = 12;
  int32_t *myPtr = &myVar;

  cout << myVar << " " << *myPtr << endl;

  *myPtr = 77;

  cout << myVar << " " << *myPtr << endl;
  change_var(myPtr);
  cout << myVar << " " << *myPtr << endl;

  return(0);
}

void change_var(int32_t *pointerToVarToChange)
{
  *pointerToVarToChange = 99;
}
