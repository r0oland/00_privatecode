#include <iostream>

using std::cout;
using std::endl;

// ------------
int main(int32_t argc, char *argv[])
{
  int16_t myVar = 17;
  int16_t *myPointer = &myVar;

  cout << myVar << " " << *myPointer << endl;

  myVar = 173;
  cout << myVar << " " << *myPointer << endl;

  *myPointer = 55;

  cout << myVar << " " << *myPointer << endl;


  return(0);
}
