#include <iostream>
#include <stdlib.h>     /* malloc, free, rand */

using std::cout;
using std::endl;

#define N_ELEMENTS 5

// ------------
int main(int32_t argc, char *argv[])
{
  int16_t *testArray = NULL;
  testArray = static_cast<int16_t*>(malloc(N_ELEMENTS*sizeof(int16_t)));
  int16_t *helpPtr = testArray; //don't want to loose base-pointer
  int16_t count = N_ELEMENTS;

  while (count--)
    *helpPtr++ = 2*count;

  helpPtr--;
  count = N_ELEMENTS;
  while (count--)
    cout << *helpPtr-- << endl;


  for (count = 0; count < N_ELEMENTS; count++)
    cout << testArray[count] << " ";
  cout << endl;

  return(0);
}
