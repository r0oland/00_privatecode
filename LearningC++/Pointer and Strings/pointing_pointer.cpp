#include <iostream>

using std::cout;
using std::endl;

int16_t myVar1_ = 0;
int16_t myVar2_ = 320;

void change_pointer(int16_t **varToChange);

// ------------
int main(int32_t argc, char *argv[])
{
  // int16_t *testPtr = NULL;
  // cout << *testPtr << endl; // crahses your program!!

  int16_t *testPtr = &myVar1_;
  cout << *testPtr << endl;

  change_pointer(&testPtr);
  cout << *testPtr << endl;

  return(0);
}

void change_pointer(int16_t **varToChange)
{
  *varToChange = &myVar2_;
}
