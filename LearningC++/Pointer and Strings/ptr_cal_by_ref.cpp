#include <iostream>
#include <stdlib.h>     /* malloc, free, rand */

using std::cout;
using std::endl;

void assign_clone(int32_t *&dst, const int32_t src[], uint32_t lenght);
// ------------
int main(int32_t argc, char *argv[])
{
  const uint32_t ARRAY_LENGTH = 10;
  int32_t *srcArray = new int32_t[ARRAY_LENGTH];
  int32_t *dstArray = 0;

  for(uint32_t count=0; count<ARRAY_LENGTH;count++)
    srcArray[count] = count;

  assign_clone(dstArray,srcArray,ARRAY_LENGTH);

  int32_t *srcIterationPtr = srcArray;
  int32_t *dstIterationPtr = dstArray;

  for(uint32_t count=0; count<ARRAY_LENGTH; count++)
    cout << "[" << *srcIterationPtr++ << "," << *dstIterationPtr++ << "]";
  cout << endl;

  delete[] srcArray;
  delete[] dstArray;
  srcArray = dstArray = 0;

  return(0);
}

// since ptr = array this works with pointers as well!
void assign_clone(int32_t *&dst, const int32_t src[], uint32_t length)
{
  dst = new int32_t[length]; //allocate memory to dst
  int32_t *dstIterationPtr = dst;
  while(length--)
    *dstIterationPtr++ = *src++;
}
