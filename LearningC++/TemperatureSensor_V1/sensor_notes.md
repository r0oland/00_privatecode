## Notes on Temperature Sensors and Temperatures Sensing
-
#### Includes
```c++
#include <time.h> /* clock, CLOCKS_PER_SEC, see time and clock examples */
#include <math.h> /* maybe needed? */
```

#### Members
- private:
  - sensor pin, uint_least8_t
  <!-- - sensor type, code? string?  -->
  - sensor properties, i.e. R, T ref, B parameter
  - last measured -> time stamp
  - measurements per second
- public:
  - total measurements, also nAverages for cum. average
  - cumulative moving average, i.e. all together until now, use good form not bruteforce
  - running average, over last nSeconds
  - min/max temperature

#### Methods:
- sample current temperature from sensor pin
- get temperature, sample n times as fast as possible to get less noisy data
- get average, either total or last n seconds

#### Class-Members
- nSensors
- total cum Average temperature
- total min max temps
- total running average - average temperatures of all sensors

#### Class-Member-Functions
- get/calc total cum -> better to just calc when needed
- get/calc total min/max -> better to just calc when needed
- get/calc total running average average
