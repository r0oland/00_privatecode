#include <cstring>
#include <cstdint>
#include <iostream>
#include <time.h>

using std::cout;
using std::endl;

#include "temperature_sensor.h"

// const variables must be defined here as well? Oh well...
// typical way of doing it:
const uint8_t TemperatureSensor::MAX_ANALOG_PIN; // Arudion Mega has 16 pins
// floats are special, can't be defined in header files...
const float TemperatureSensor::AMB_TEMP = 298.15; //room temperature in Kelvin (25 Deg default) def = 298.15

// init when constructing instances?
float TemperatureSensor::TotalCumAverageTemp = -128;
float TemperatureSensor::TotalRunAverageTemp = -128;
float TemperatureSensor::TotalMinTemp = -128;
float TemperatureSensor::TotalMaxTemp = -128;
uint8_t TemperatureSensor::NSensors = 0;
uint8_t TemperatureSensor::SecondsForRunAverage = 10;
uint16_t TemperatureSensor::SamplesPerSecond = 10;

//------------------------------------------------------------------------------
// constructor of MemoryGameCard using default parameters for most things
TemperatureSensor::TemperatureSensor( uint8_t sensorPin, uint16_t bParameter)
{
  NSensors++;
  if(sensorPin > MAX_ANALOG_PIN)
  {
    // through an exeption here?
    return;
  }
  SensorPin = sensorPin;
  cout << "Init with sensor pin: " << SensorPin << endl;
  BParameter = bParameter;
  cout << "Init with bParameter: " << BParameter << endl;
  RefRes = 10000;
  NomRes = 10000;
  lastClock = clock();

  cout << "constants are here as well: " << TemperatureSensor::MAX_ANALOG_PIN;
  cout << " and " << TemperatureSensor::AMB_TEMP << endl;
}

//------------------------------------------------------------------------------
// constructor of MemoryGameCard using more parameters
TemperatureSensor::TemperatureSensor( uint8_t sensorPin, uint16_t bParameter,
                                      uint16_t refRes, uint16_t nomRes)
{
  NSensors++;
  if(sensorPin > MAX_ANALOG_PIN)
  {
    // through an exeption here?
    return;
  }
  SensorPin = sensorPin;
  BParameter = bParameter;
  RefRes = refRes;
  NomRes = nomRes;
  lastClock = clock();
}


//------------------------------------------------------------------------------
// destructor of MemoryGameCard
TemperatureSensor::~TemperatureSensor()
{
  cout << "Destructor has been called " << endl;
  NSensors--;
}
//------------------------------------------------------------------------------

float TemperatureSensor::Get_CurrentTemp()
{
  return(0);
}
//------------------------------------------------------------------------------

float TemperatureSensor::Get_CumAverageTemp()
{
  return(0);
}
//------------------------------------------------------------------------------

float TemperatureSensor::Get_RunAverageTemp()
{
  return(0);
}
//------------------------------------------------------------------------------

float TemperatureSensor::Get_MinTemp()
{
  return(0);
}
//------------------------------------------------------------------------------

float TemperatureSensor::Get_MaxTemp()
{
  return(0);
}
//------------------------------------------------------------------------------

uint32_t TemperatureSensor::Get_NMeasurements()
{
  return(0);
}

//------------------------------------------------------------------------------
float TemperatureSensor::Get_TotalCumAverageTemp()
{
  return(0);
}

//------------------------------------------------------------------------------
float TemperatureSensor::Get_TotalRunAverageTemp()
{
  return(0);
}

//------------------------------------------------------------------------------
float TemperatureSensor::Get_TotalMinTemp()
{
  return(0);
}

//------------------------------------------------------------------------------
float TemperatureSensor::Get_TotalMaxTemp()
{
  return(0);
}

//------------------------------------------------------------------------------
uint8_t TemperatureSensor::Get_NSensors()
{
  return(NSensors);
}

//------------------------------------------------------------------------------
uint8_t TemperatureSensor::Get_SecondsForRunAverage()
{
  return(0);
}

//------------------------------------------------------------------------------
TemperatureSensor::Set_SecondsForRunAverage(uint8_t secondsForRunAverage)
{
  return(0);
}

//------------------------------------------------------------------------------
uint16_t TemperatureSensor::Get_SamplesPerSecond()
{
  return(0);
}

//------------------------------------------------------------------------------
TemperatureSensor::Set_SamplesPerSecond(uint16_t samplesPerSecond)
{
  return(0);
}
