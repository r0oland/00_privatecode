#ifndef temperature_sensor_h___
#define temperature_sensor_h___

// 􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀􀀀MemoryGameCard declaration file as it should be!
// model of a card as used in the game "memory"

class TemperatureSensor
{
protected:
  uint8_t SensorPin; //Analog pin of the sensor
  uint16_t BParameter; //Type of sensor defined by bParameter
  uint16_t RefRes; //Resistance of Reference resistor, def 10000
  uint16_t NomRes; //Nominal Resitance of Therm at 25deg C. def 10000
  uint16_t lastClock;

public:
  float CurrentTemp;
  float CumAverageTemp;
  float RunAverageTemp;
  float MinTemp;
  float MaxTemp;
  uint32_t NMeasurements;

  static float TotalCumAverageTemp;
  static float TotalRunAverageTemp;
  static float TotalMinTemp;
  static float TotalMaxTemp;
  static uint8_t NSensors;
  static uint8_t SecondsForRunAverage;
  static uint16_t SamplesPerSecond;

  // constants must still be defined in the .cpp file, look there
  static const uint8_t MAX_ANALOG_PIN = 15; // Arudion Mega has 16 pins
  static const float AMB_TEMP; //room temperature in Kelvin (25 Deg default) def = 298.15

  TemperatureSensor(uint8_t sensorPin, uint16_t bParameter);
  TemperatureSensor(uint8_t sensorPin, uint16_t bParameter,
                    uint16_t refRes, uint16_t nomRes);
  ~TemperatureSensor();

  float Get_CurrentTemp();
  float Get_CumAverageTemp();
  float Get_RunAverageTemp();
  float Get_MinTemp();
  float Get_MaxTemp();
  uint32_t Get_NMeasurements();

  static float Get_TotalCumAverageTemp();
  static float Get_TotalRunAverageTemp();
  static float Get_TotalMinTemp();
  static float Get_TotalMaxTemp();
  static uint8_t Get_NSensors();
  static uint8_t Get_SecondsForRunAverage();
  static Set_SecondsForRunAverage(uint8_t secondsForRunAverage);
  static uint16_t Get_SamplesPerSecond();
  static Set_SamplesPerSecond(uint16_t samplesPerSecond);

};

#endif
