//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Includes
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#include <LiquidCrystal.h>
#include <Stepper.h>
#include <stdio.h>

#include <DHT.h>

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// LCD Display Setup
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(22, 23, 25, 26, 27, 28);
char buffer[6]=""; //buffer for lcd string 

//%%%%%%%%%%%%%%%%%%%%%%%%%%%s%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// DHT Sensor Setup
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
#define DHT_PIN1 24     // what pin we're connected to
#define DHT_PIN2 39     // what pin we're connected to
#define DHT_TYPE DHT22   // DHT 22  (AM2302)

// Initialize DHT sensor
DHT dht1(DHT_PIN1, DHT_TYPE);
DHT dht2(DHT_PIN2, DHT_TYPE);


//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Stepper Motor Setup 
//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// Haydon Switch Steppers -> 12V!!!
// #define STEPS 360/7.5 //(360/7.5 = 48 steps for one roundtrip)

// SM 42051 Stepper -> 4.5V!!!
// #define STEPS 360/1.8 //(360/7.5 = 48 steps for one roundtrip)

// CD 1 stepper, 5V, max/opt speed = 1500
// #define STEPS 20 //(360/18 = 20 steps for one roundtrip)

// CD 2 stepper, 5V, max/opt speed = 1500
// CD 2 stepper, 12V, max/opt speed = 3000
#define STEPS 20 //(360/18 = 20 steps for one roundtrip)

// printer stepper 1, 5V, max/opt speed = 150
#define STEPS 100 //

Stepper stepper(STEPS, 29, 30, 31, 32);



//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void setup() {
  // setup lcd
  lcd.begin(25, 4);
  lcd.print("ArduMegaAllThings");

  // setup stepper 
  stepper.setSpeed(300);

  dht1.begin();
  dht2.begin();

}

//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
void loop() {


// long currentStep;

  // for(int iSteps=5; iSteps<20; iSteps++){
  //   lcd.setCursor(0, 1);
  //   lcd.print(millis()/1000);

  //   currentStep = STEPS/(20-iSteps);
  //   lcd.setCursor(0, 2);
  //   lcd.print("stepping: ");
  //   sprintf(buffer, "%04d", currentStep);
  //   lcd.print(buffer);
  //   stepper.step(currentStep);

  //   lcd.setCursor(0, 2);
  //   lcd.print("stepping: ");
  //   sprintf(buffer, "%04d", -currentStep);
  //   lcd.print(buffer);
  //   stepper.step(-currentStep);
  // }

  // for(int iSteps=1; iSteps<10; iSteps++){
  //   stepper.step(STEPS);
  //   stepper.step(-STEPS);
  // }
// for(int i=0; i<15; i++){
//   stepper.step(10*i);
//   stepper.step(-10*i);
// }

  // stepper.step(1000);
  // delay(1000);
  // stepper.step(-10000);

  delay(1500);

  // Reading temperature or humidity takes about 250 milliseconds!
  // Sensor readings may also be up to 2 seconds 'old' (its a very slow sensor)
  float h = dht1.readHumidity();
  // Read temperature as Celsius
  float t = dht1.readTemperature();
  // Read temperature as Fahrenheit
  float f = dht1.readTemperature(true);


    if (isnan(h) || isnan(t) || isnan(f)) {
    Serial.println("Failed to read from DHT sensor!");
    return;
  }

    // Compute heat index
  // Must send in temp in Fahrenheit!
  float hi = dht1.computeHeatIndex(f, h);
  
  lcd.setCursor(0, 0);
  lcd.print("Humidity: ");
  lcd.print(h);
  lcd.print(" %   ");

  lcd.print("Temperature: "); 
  lcd.print(t);
  lcd.print("*C");


  Serial.print("Humidity: "); 
  Serial.print(h);
  Serial.print(" %\t");
  Serial.print("Temperature: "); 
  Serial.print(t);
  Serial.print("*C");
  Serial.print(f);
  Serial.print(" *F\t");
  Serial.print("Heat index: ");
  Serial.print(hi);
  Serial.println(" *F");


}

